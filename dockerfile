FROM python:latest

WORKDIR /app

RUN apt-get update && apt-get install -y python3-pip
RUN pip install Flask==2.0.1 Werkzeug==2.2.2

EXPOSE 5000

COPY . /app

CMD ["python", "PWA.py"]